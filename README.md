# Section 32: Bonus: TypeScript Ingroduction

## s32-450 Installing & Using TypeScript

```
npm install typescript
npx tsc with-typescript.ts
```

## s32-460 Configuring the TypeScript Compiler

```
#npx tsc
npx tsc with-typescript.ts

# creates tsconfig.json in the same directory
npx tsc --init 
```